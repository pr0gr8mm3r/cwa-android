package de.rki.coronawarnapp.util.bluetooth

import android.bluetooth.BluetoothAdapter
import android.os.Build

/**
 * From microG ScannerService
 */
fun isScannerSupported(): Boolean? {
    val adapter = BluetoothAdapter.getDefaultAdapter()
    return when {
        adapter == null -> false
        adapter.state != BluetoothAdapter.STATE_ON -> null
        adapter.bluetoothLeScanner != null -> true
        else -> false
    }
}

/**
 * From microG AdvertiserService (private class)
 */
fun isAdvertisingSupported(): Boolean? {
    val adapter = BluetoothAdapter.getDefaultAdapter()
    return when {
        adapter == null -> false
        Build.VERSION.SDK_INT >= 26 && (adapter.isLeExtendedAdvertisingSupported || adapter.isLePeriodicAdvertisingSupported) -> true
        adapter.state != BluetoothAdapter.STATE_ON -> null
        adapter.bluetoothLeAdvertiser != null -> true
        else -> false
    }
}
